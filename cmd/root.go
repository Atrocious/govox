package cmd

import (
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/markbates/pkger"

	"github.com/spf13/cobra"
	"gitlab.com/Atrocious/govox/audio"
)

var (
	list       bool
	completion string
	device     string

	rootCmd = &cobra.Command{
		Use:               "govox",
		Short:             "govox a VOX tool for your pc",
		Long:              `govox is a command tool that makes your computer speak what you type with the Half-Life Announcer voice or VOX`,
		Run:               run,
		ValidArgsFunction: validArgs,
	}

	voxWords words
)

type words map[string]string

func (w words) contains(c string) bool {
	for k := range w {
		if k == c {
			return true
		}
	}
	return false
}

func (w words) pop(key string) (string, bool) {
	v, ok := w[key]
	if ok {
		delete(w, key)
	}
	return v, ok
}

// init cmd, must have declared the VoxBox before calling
func init() {
	// Create a flag for the command to list all available words
	rootCmd.PersistentFlags().BoolVarP(&list, "list", "l", false, "list available vox words")
	rootCmd.PersistentFlags().StringVarP(&completion, "validArgs", "c", "", "generate completions for the shell")
	rootCmd.PersistentFlags().StringVarP(&device, "device", "d", "", "playback device to play to")
}

func initWords() {
	voxWords = make(words)
	_ = pkger.Walk("/vox", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		name := info.Name()
		voxWords[strings.Replace(name, ".wav", "", 1)] = path
		return nil
	})
}

// run is the main command function
func run(cmd *cobra.Command, args []string) {
	initWords()

	comma, ok := voxWords.pop("_comma")
	if ok {
		voxWords[","] = comma
	}
	// If the list flag is used then only list the available words then exit
	if list {
		for _, s := range listWords() {
			fmt.Println(s)
		}
		return
	}

	if completion != "" {
		genCompletions(cmd)
		return
	}

	// If there are no words to say then exit
	if len(args) < 1 {
		return
	}

	// Find commas attached to words and insert them as their own arg
	for i, arg := range args {
		if strings.Contains(arg, ",") && arg != "," {
			args[i] = ","
			args = append(args, "")
			copy(args[i+1:], args[i:])
			args[i] = strings.Replace(arg, ",", "", 1)
		}
	}

	// Create a list of word paths based on the user's input
	words := make([]string, len(args))
	i := 0
	for _, arg := range args {
		if voxWords.contains(arg) {
			words[i] = voxWords[arg]
			i++
		} else {
			// The user has entered a word that cannot be said
			// tell them which word is not valid, and how to get a list of available words
			fmt.Printf("%s is not a valid vox word\n", arg)
			fmt.Println("run govox -l for a list of valid words")
			return
		}
	}

	// Playback each word one after another
	for _, word := range words {
		file, err := pkger.Open(word)
		if err != nil {
			fmt.Println(word)
			panic(err)
		}
		// This play call is blocking
		if device != "" {
			_ = audio.PlayOnDevice(file, device)
		} else {
			_ = audio.Play(file)
		}
	}
}

// listWords prints a list of all available words
func listWords() []string {
	words := make([]string, len(voxWords))
	i := 0
	for k := range voxWords {
		words[i] = k
		i++
	}
	sort.Strings(words)
	return words
}

// possibleCompletions list possible completions for the string entered
func possibleCompletions(toComplete string) []string {
	var words []string
	for _, word := range listWords() {
		if strings.HasPrefix(word, toComplete) {
			words = append(words, word)
		}
	}
	return words
}

// validArgs is the function used by cobra's ValidArgsFunction to provide dynamic noun completions
func validArgs(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	return possibleCompletions(toComplete), cobra.ShellCompDirectiveNoFileComp
}

func genCompletions(cmd *cobra.Command) {
	switch completion {
	case "bash":
		_ = cmd.Root().GenBashCompletion(os.Stdout)
	case "zsh":
		_ = cmd.Root().GenZshCompletion(os.Stdout)
	case "fish":
		_ = cmd.Root().GenFishCompletion(os.Stdout, true)
	case "powershell":
		_ = cmd.Root().GenPowerShellCompletion(os.Stdout)
	}
}

// Execute executes the command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
