# Voicemod Tutorial

So you want to amaze (or annoy) your friends in voice chat. Good.

## Setup
1. Install voicemod from [here](https://www.voicemod.net/)
2. Install govox following instructions [here](https://gitlab.com/Atrocious/govox/-/blob/master/README.md)

## Run
1. Open the control panel
2. Navigate to `Hardware and Sound -> Sound`
3. Make sure you are on the __Playback__ tab
4. Find the name of the Voicemod Virtual Audio Device
5. Run __govox__ with the __-d__ option as such `govox -d <name of Voicemod Virtual Audio Device> [words vox will say]`
