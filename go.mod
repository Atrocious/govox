module gitlab.com/Atrocious/govox

go 1.12

require (
	github.com/gen2brain/malgo v0.10.18
	github.com/gobuffalo/here v0.6.2 // indirect
	github.com/markbates/pkger v0.17.1
	github.com/spf13/cobra v1.0.0
	github.com/youpy/go-wav v0.1.0
)
