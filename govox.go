package main

import (
	"gitlab.com/Atrocious/govox/cmd"
)

func init() {
	// ensure pkger knows that we want to package the vox files
	// _ = pkger.Include("/vox")
}

func main() {
	cmd.Execute()
}
