# govox
Half Life one vox console for your computer written in golang. This program uses
vox files from the awesome [Half-Life VOX Console](https://github.com/suchipi/half-life-vox-console)
project by [suchipi](https://github.com/suchipi).

## Installation
Currently the best way to install this [install go](https://golang.org/doc/install)
and run the command `go get -u gitlab.com/Atrocious/govox`.


## Usage
Using govox is simple, just open a terminal and run `govox [what you want vox to say]`.

govox can generate autocompletions for your convenience by using `govox -c [name of your shell]`

you can also specify an audio device to output the audio to `govox -d [device name] [what you want vox to say]`. This can be used to play the output to [voicemod](https://www.voicemod.net/) which in turn can play out of your mic.

Vox has a specific list of words that it can say, to get that list you can run
`govox -l` which will print an alphabetical list of all the words vox can say.

#### Examples
`govox hello gordon freeman`

`govox this area is under maximum lockout`

`govox woop woop your ass is a biohazard woop woop`

##### Generate completions
`govox -c bash`

`govox -c powershell`

##### Set device
`govox -d Voicemod hello gordon freeman`

When specifying the playback device you do not have to use the full device name.
As long as the device name is equal to OR contains what you specify it will select
the first device that matches on that name. If you need more help check the [voicemod tutorial](https://gitlab.com/Atrocious/govox/-/blob/master/voicemod-tutorial.md)
