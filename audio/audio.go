package audio

import "C"
import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	"github.com/markbates/pkger/pkging"

	"github.com/gen2brain/malgo"
	wav "github.com/youpy/go-wav"
)

// play performs the actual playback in blocking mode
func play(channel chan error, context malgo.Context, deviceConfig malgo.DeviceConfig, deviceCallbacks malgo.DeviceCallbacks) error {

	device, err := malgo.InitDevice(context, deviceConfig, deviceCallbacks)
	if err != nil {
		return err
	}
	defer device.Uninit()

	err = device.Start()
	if err != nil {
		return err
	}

	// block until we get a response from the deviceCallbacks
	err = <-channel
	return err
}

// callbacks returns a malgo.DeviceCallbacks and a chan error that can be used to make blocking playback
func callbacks(reader io.Reader) (malgo.DeviceCallbacks, chan error) {

	abortChan := make(chan error)
	aborted := false

	return malgo.DeviceCallbacks{
		Data: func(outputSamples, inputSamples []byte, frameCount uint32) {
			if aborted {
				return
			}
			if frameCount == 0 {
				return
			}

			read, err := io.ReadFull(reader, outputSamples)
			if read <= 0 {
				if err != nil {
					aborted = true
					abortChan <- err
				}
				return
			}
		},
	}, abortChan
}

// devices returns a playback and capture device of the specified name from the context
// may return one or no devices
func devices(ctx malgo.Context, name string) (*malgo.DeviceInfo, *malgo.DeviceInfo, error) {
	var playbackInfo *malgo.DeviceInfo
	devices, err := ctx.Devices(malgo.Playback)
	if err != nil {
		return nil, nil, err
	}
	for _, info := range devices {
		if strings.Contains(info.Name(), name) || strings.EqualFold(info.Name(), name) {
			playbackInfo = &info
			break
		}
	}
	if playbackInfo == nil {
		return nil, nil, fmt.Errorf("No playback device containing or matching: '%s' found", name)
	}

	var captureInfo *malgo.DeviceInfo
	devices, err = ctx.Devices(malgo.Capture)
	if err != nil {
		return nil, nil, err
	}
	for _, info := range devices {
		if strings.Contains(info.Name(), name) || strings.EqualFold(info.Name(), name) {
			captureInfo = &info
		}
	}
	if captureInfo == nil {
		return nil, nil, fmt.Errorf("No capture device containing or matching: '%s' found", name)
	}
	return playbackInfo, captureInfo, nil
}

// Play play a file to default audio playback
func Play(file pkging.File) error {
	data, err := ioutil.ReadAll(file)

	reader := wav.NewReader(bytes.NewReader(data))
	format, err := reader.Format()
	if err != nil {
		return err
	}

	deviceCallbacks, channel := callbacks(reader)

	ctx, err := malgo.InitContext(nil, malgo.ContextConfig{}, func(message string) {})
	if err != nil {
		return err
	}

	defer func() {
		_ = ctx.Uninit()
		ctx.Free()
		close(channel)
	}()

	deviceConfig := malgo.DeviceConfig{
		DeviceType: malgo.Playback,
		SampleRate: format.SampleRate,
		Playback: malgo.SubConfig{
			Format:   malgo.FormatU8,
			Channels: uint32(format.NumChannels),
		},
	}

	return play(channel, ctx.Context, deviceConfig, deviceCallbacks)
}

// PlayOnDevice play a file on a playback device matching deviceName
func PlayOnDevice(file pkging.File, deviceName string) error {
	defer file.Close()
	data, err := ioutil.ReadAll(file)

	reader := wav.NewReader(bytes.NewReader(data))
	format, err := reader.Format()
	if err != nil {
		return err
	}

	deviceCallbacks, channel := callbacks(reader)

	ctx, err := malgo.InitContext(nil, malgo.ContextConfig{}, func(message string) {})
	if err != nil {
		return err
	}

	defer func() {
		_ = ctx.Uninit()
		ctx.Free()
		close(channel)
	}()

	playback, capture, err := devices(ctx.Context, deviceName)
	if err != nil {
		return err
	}

	deviceConfig := malgo.DeviceConfig{
		DeviceType: malgo.Playback,
		SampleRate: format.SampleRate,
		Playback: malgo.SubConfig{
			Format:   malgo.FormatU8,
			DeviceID: playback.ID.Pointer(),
			Channels: uint32(format.NumChannels),
		},
		Capture: malgo.SubConfig{
			Format:   malgo.FormatU8,
			DeviceID: capture.ID.Pointer(),
			Channels: uint32(format.NumChannels),
		},
	}

	return play(channel, ctx.Context, deviceConfig, deviceCallbacks)
}
